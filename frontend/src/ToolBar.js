import './App.css';
import { observer } from 'mobx-react-lite';
import { CurrentPageType, loadProcessList } from './state';

const ToolBar = observer(({currentPage, currentUser}) => {

    return (
        <div className='toolBar'>
            <input id="search" placeholder='Search' />
            <button className='toolBarItem'>Search</button>
            <button className='toolBarItem' onClick={() => loadProcessList(currentUser.user)}>Refresh</button>
            <button className='toolBarItem' onClick={() => currentPage.setCurrent(CurrentPageType.CreateForm)}>Create</button>
            <button className='toolBarItem' onClick={() => currentUser.setUser(null)}>Logout</button>
        </div>
    )
})

export default ToolBar