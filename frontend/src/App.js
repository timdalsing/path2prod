import './App.css';
import { observer } from 'mobx-react-lite';
import ToolBar from './ToolBar';
import ProcessList from './ProcessList';
import CreateForm from './CreateForm';
import { currentPageStore, CurrentPageType, processListStore, currentUserStore, loadGroupNames, loadNamespaces, loadProcessList } from './state';
import LoginForm from './Login';

const Body = observer(({ currentPage }) => {

  if (currentPage.current === CurrentPageType.ProcessList) {

    const user = currentUserStore.user
    loadProcessList(user)
  
    return (
      <ProcessList processList={processListStore} />
    )
  } else {
    loadGroupNames()
    loadNamespaces()

    return (
      <CreateForm />
    )
  }
})

const Main = observer(({ currentPage, currentUser }) => {

  if (currentUser.user === null) {
    return (
      <LoginForm currentUser={currentUser} />
    )
  } else {
    return (
      <div className="Main">
        <ToolBar currentPage={currentPage} currentUser={currentUser} />
        <div className='content'>
          <Body currentPage={currentPage} />
        </div>
      </div>
    )
  }
})

const Header = observer(({ currentUser }) => {
  if (currentUser.user === null) {
    return (
      <div className='header'>
        <div className='title'>Path 2 Prod</div>
      </div>
    )
  } else {
    return (
      <div className='header'>
        <div className='title'>Path 2 Prod</div>
        <div className='user'>User: {currentUser.user}</div>
      </div>
    )
  }
})

function App() {

  return (
    <div className="App">
      <Header currentUser={currentUserStore} />
      <Main currentPage={currentPageStore} currentUser={currentUserStore} />
    </div>
  )
}

export default App;
