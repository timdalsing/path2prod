import './App.css';
import { observer } from 'mobx-react-lite';
import { namespacesStore, selectedNamespaceStore } from './state';

const NamespaceList = observer(({ namespaces }) => {

    const selectNamespace = (namespace) => {
        selectedNamespaceStore.setSelectedNamespace(namespace)
    }

    const NamespaceItem = ({ namespace }) => {
        return (
            <option value={namespace}>{namespace}</option>
        )
    }

    return (
        <select className='formField' placeholder='Select Namespace' onChange={(event) => selectNamespace(event.target.value)}>
            <option value={null}>Select Namespace</option>
            {namespaces.namespaces.map(n => <NamespaceItem key={n} namespace={n} />)}
        </select>
    )
})

const NamespaceField = () => {

    return (
        <div className='formRow'>
            <div className='formTitle'>Namespace</div>
            <NamespaceList namespaces={namespacesStore} />
        </div>
    )
}

export default NamespaceField