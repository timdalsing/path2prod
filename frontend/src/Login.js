import './App.css'
import { useState } from 'react'
import { observer } from 'mobx-react-lite'

const LoginForm = observer(({ currentUser }) => {

    const [user, setUser] = useState('')
    const [password, setPassword] = useState('')

    const login = () => {
        currentUser.setUser(user)
    }

    const isDisabled = () => {
        return user.length === 0 || password.length === 0
    }

    return (
        <div className="createForm" style={{ margin: '10px', padding: '10px' }}>
            <div className='formRow'>
                <div className='formTitle'>User Name</div>
                <input 
                    className='formField'
                    type='text' 
                    placeholder='User Name' 
                    value={user} 
                    onChange={(event) => setUser(event.target.value)} />
            </div>
            <div className='formRow'>
                <div className='formTitle'>Password</div>
                <input 
                    className='formField' 
                    type='password' 
                    placeholder='Password' 
                    value={password} 
                    onChange={(event) => setPassword(event.target.value)} />
            </div>
            <div className='formRow' style={{ justifyContent: 'flex-end', paddingTop: '10px' }}>
                <button type='submit' disabled={isDisabled()} onClick={login}>Login</button>
            </div>
        </div>
    )
})

export default LoginForm