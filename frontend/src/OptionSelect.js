import './App.css'
import { useState } from 'react'

const OptionSelect = ({ options }) => {

    const [values, setValues] = useState([
        {
            value: 'actuator',
            checked: false,
            title: 'Actuator'
        },
        {
            value: 'sleuth',
            checked: false,
            title: 'Sleuth'
        }
    ])
    
    const changeValue = (value) => {
        const newValues = values.map(t => {
            if (t.value === value) {
                t.checked = !t.checked
            }
            return t
        })
        setValues(newValues)
        
        const selected = newValues.filter(t => t.checked).map(t => t.value)
        options.setOptions(selected)
    }
    
    const Item = ({ value, title, checked, callback }) => {
        return (
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div>
                    <input type="checkbox" title={title} value={value} checked={checked} onChange={callback} />
                </div>
                <div>{title}</div>
            </div>
        )
    }

    return (
        <div className='formRow'>
            <div className='formTitle'>Options</div>
            <div className='formField' style={{ display: 'flex', flexDirection: 'column', borderColor: 'black', borderWidth: '1px', borderStyle: 'solid' }}>
                {values.map(t => <Item key={t.value} value={t.value} title={t.title} checked={t.checked} callback={() => changeValue(t.value)} />)}
            </div>
        </div>
    )
}

export default OptionSelect
