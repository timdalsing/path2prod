import './App.css'
import { useState } from 'react'
import { api } from './api'
import { 
    currentPageStore, 
    CurrentPageType, 
    currentUserStore, 
    selectedGroupStore, 
    selectedNamespaceStore, 
    projectStore, 
    packageNameStore, 
    emailStore,
    typeStore,
    optionsStore,
    servicesStore
} from './state'
import GroupField from './GroupField'
import NamespaceField from './NamespaceField'
import { observer } from 'mobx-react-lite'
import InputField from './InputField'
import TypeSelect from './TypeSelect'
import OptionSelect from './OptionSelect'
import ServicesSelect from './ServicesSelect'

const SubmitButton = observer(({ selectedGroup, selectedNamespace, project, packageName, email, type, handleSubmit }) => {

    const disabled =
        project.value === '' ||
        packageName.value === '' ||
        email.value === '' ||
        selectedGroup.group === null ||
        type.value === '' ||
        selectedNamespace.namespace === null

    return (
        <button className='formButton' type="submit" onClick={() => handleSubmit()} disabled={disabled}>Create</button>
    )
})

const CreateForm = () => {

    typeStore.setValue('')
    servicesStore.setServices([])
    optionsStore.setOptions([])

    const handleSubmit = () => {

        const vars = {
            groupName: selectedGroupStore.group,
            namespace: selectedNamespaceStore.namespace,
            projectName: projectStore.value,
            packageName: packageNameStore.value,
            username: currentUserStore.user,
            email: emailStore.value,
            type: typeStore.value,
            options: optionsStore.options,
            services: servicesStore.services
        }
        console.log(`CreateForm.handleSubmit: vars = ${JSON.stringify(vars)}`)

        api
            .post('/process', vars)
            .then(resp => {
                const data = resp.data
                console.log(`CreateForm.handleSubmit: successful`)
            })
            .catch(err => console.log(`CreateForm.handleSubmit: err = ${JSON.stringify(err)}`))
            .finally(() => {
                currentPageStore.setCurrent(CurrentPageType.ProcessList)
            })
    }

    const handleCancel = () => {
        currentPageStore.setCurrent(CurrentPageType.ProcessList)
    }

    return (
        <div className="createForm">
            <GroupField />
            <NamespaceField />
            <InputField title='Project' id='project' type='text' placeholder='Project' field={projectStore} />
            <InputField title='Package' id='package' type='text' placeholder='Package' field={packageNameStore} />
            <InputField title='Email' id='email' type='text' placeholder='Email' field={emailStore} />
            <TypeSelect type={typeStore} />
            <OptionSelect options={optionsStore} />
            <ServicesSelect services={servicesStore} />
            <div className='formRow' style={{ justifyContent: 'flex-end', paddingTop: '10px' }}>
                <SubmitButton
                    selectedGroup={selectedGroupStore}
                    selectedNamespace={selectedNamespaceStore}
                    project={projectStore}
                    packageName={packageNameStore}
                    email={emailStore}
                    type={typeStore}
                    handleSubmit={handleSubmit} />
                <button className='formButton' type="submit" onClick={() => handleCancel()}>Cancel</button>
            </div>
        </div>
    )
}

export default CreateForm
