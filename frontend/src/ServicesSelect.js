import './App.css'
import { useState } from 'react'

const ServicesSelect = ({ services }) => {

    const [values, setValues] = useState([
        {
            value: 'postgresql',
            checked: false,
            title: 'PostgreSQL'
        }
    ])

    const Item = ({ value, title, checked, callback }) => {
        return (
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div>
                    <input type="checkbox" title={title} value={value} checked={checked} onChange={callback} />
                </div>
                <div>{title}</div>
            </div>
        )
    }

    const changeValue = (value) => {
        const newValues = values.map(t => {
            if (t.value === value) {
                t.checked = !t.checked
            }
            return t
        })
        setValues(newValues)

        const selected = newValues.filter(t => t.checked).map(t => t.value)
        services.setServices(selected)
    }

    return (
        <div className='formRow'>
            <div className='formTitle'>Services</div>
            <div className='formField' style={{ display: 'flex', flexDirection: 'column', borderColor: 'black', borderWidth: '1px', borderStyle: 'solid' }}>
                {values.map(t => <Item key={t.value} value={t.value} title={t.title} checked={t.checked} callback={() => changeValue(t.value)} />)}
            </div>
        </div>
    )
}

export default ServicesSelect
