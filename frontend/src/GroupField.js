import './App.css';
import { observer } from 'mobx-react-lite';
import { groupNamesStore, selectedGroupStore } from './state';

const GroupList = observer(({ groupNames }) => {

    const selectGroup = (group) => {
        selectedGroupStore.setSelectedGroup(group)
    }

    const GroupItem = ({ group }) => {
        return (
            <option value={group}>{group}</option>
        )
    }

    return (
        <select className='formField' placeholder='Select Group' onChange={(event) => selectGroup(event.target.value)}>
            <option value={null}>Select Group</option>
            {groupNames.names.map(g => <GroupItem key={g} group={g} />)}
        </select>
    )
})

const GroupField = () => {

    return (
        <div className='formRow'>
            <div className='formTitle'>Group</div>
            <GroupList groupNames={groupNamesStore} />
        </div>
    )
}

export default GroupField