import { makeAutoObservable } from "mobx"
import { api } from "./api"

class Services {
    constructor() {
        this.services = []
        makeAutoObservable(this)
    }

    setServices(services) {
        this.services = services
    }
}

export const servicesStore = new Services()

class Options {
    constructor() {
        this.options = []
        makeAutoObservable(this)
    }

    setOptions(options) {
        this.options = options
    }
}

export const optionsStore = new Options()

class FieldValue {
    constructor() {
        this.value = ''
        makeAutoObservable(this)
    }

    setValue(value) {
        this.value = value
    }
}

export const projectStore = new FieldValue()
export const packageNameStore = new FieldValue()
export const emailStore = new FieldValue()
export const typeStore = new FieldValue()

class SelectedNamespace {
    constructor() {
        this.namespace = null
        makeAutoObservable(this)
    }

    setSelectedNamespace(namespace) {
        this.namespace = namespace
    }
}

export const selectedNamespaceStore = new SelectedNamespace()

class Namespaces {
    constructor() {
        this.namespaces = []
        makeAutoObservable(this)
    }

    setNamespaces(namespaces) {
        this.namespaces = namespaces
    }
}

export const namespacesStore = new Namespaces()

export const loadNamespaces = () => {
    console.log(`loadNamespaces`)
    api
        .get('/kubernetes/namespace')
        .then(resp => {
            const data = resp.data
            console.log(`loadNamespaces: data = ${JSON.stringify(data)}`)
            namespacesStore.setNamespaces(data)
        })
        .catch(err => console.log(`loadNamespaces: err = ${JSON.stringify(err)}`))
}

class GroupNames {
    constructor() {
        this.names = []
        makeAutoObservable(this)
    }

    setNames(names) {
        this.names = names
    }
}

export const groupNamesStore = new GroupNames()

export const loadGroupNames = () => {
    console.log(`loadGroupNames`)
    api
        .get('/gitlab/groupnames')
        .then(resp => {
            const data = resp.data
            console.log(`loadGroupNames: data = ${JSON.stringify(data)}`)
            groupNamesStore.setNames(data)
        })
        .catch(err => console.log(`loadGroupNames: err = ${JSON.stringify(err)}`))
}

class SelectedGroup {
    constructor() {
        this.group = null
        makeAutoObservable(this)
    }

    setSelectedGroup(group) {
        this.group = group
    }
}

export const selectedGroupStore = new SelectedGroup()

export class CurrentUser {
    constructor() {
        this.user = null
        makeAutoObservable(this)
    }

    setUser(user) {
        this.user = user
    }
}

export const currentUserStore = new CurrentUser()

export const CurrentPageType = {
    ProcessList: "ProcessList",
    CreateForm: "CreateForm"
}

export class CurrentPage {
    constructor() {
        this.current = CurrentPageType.ProcessList
        makeAutoObservable(this)
    }

    setCurrent(current) {
        this.current = current
    }
}

export const currentPageStore = new CurrentPage()

export class ProcessList {
    constructor() {
        this.list = []
        makeAutoObservable(this)
    }

    setList(list) {
        this.list = list
    }
}

export const processListStore = new ProcessList()

export const loadProcessList = (user) => {
    console.log(`loadProcessList: user = ${user}`)
    api
        .get(`/process/${user}`)
        .then(resp => {
            const data = resp.data
            console.log(`loadProcessList: user = ${user}, data = ${JSON.stringify(data)}`)
            processListStore.setList(data)
        })
        .catch(err => console.log(`loadProcessList: err = ${JSON.stringify(err)}`))
}