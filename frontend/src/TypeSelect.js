import './App.css'

const TypeSelect = ({ type }) => {
    return (
        <div className='formRow'>
            <div className='formTitle'>Application Type</div>
            <select className='formField' onChange={(event) => type.setValue(event.target.value)}>
                <option value=''>Select Application Type</option>
                <option value='web'>Web</option>
                <option value='webflux'>Webflux</option>
                <option value='stream_kafka'>Spring Cloud Stream Kafka</option>
                <option value='stream_rabbit'>Spring Cloud Stream RabbitMQ</option>
            </select>
        </div>
    )
}

export default TypeSelect
