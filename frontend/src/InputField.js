import './App.css'
import { useState } from 'react'

const InputField = ({ title, id, type, placeholder, field }) => {

    const [value, setValue] = useState('')

    const onChangeValue = (newValue) => {
        setValue(newValue)
        field.setValue(newValue)
    }

    return (
        <div className='formRow'>
            <div className='formTitle'>{title}</div>
            <input
                className='formField'
                id={id}
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={(event) => onChangeValue(event.target.value)} />
        </div>
    )
}

export default InputField
