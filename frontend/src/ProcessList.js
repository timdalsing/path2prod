import './App.css';
import { observer } from 'mobx-react-lite';

const StatusItem = ({ processInfo }) => {

  const generateTooltip = () => {
    return processInfo.resultCode + ' (' + processInfo.resultDescription + ')'
  }

  if (processInfo.status === 'SUCCESS') {
    return <td style={{ color: 'green' }}>{processInfo.status}</td>
  } else {
    return (
      <td title={generateTooltip()} style={{ color: 'red', fontWeight: 'bold' }}>{processInfo.status}</td>
    )
  }
}

const ProcessListItem = ({ processInfo, count }) => {

  const vars = processInfo.processVariables
  const bgColor = count % 2 == 0 ? 'lightblue' : 'white'

  return (
    <tr style={{ backgroundColor: bgColor }}>
      <td style={{ fontWeight: 'bold' }}>{processInfo.processInstanceId}</td>
      <td>{processInfo.startTime}</td>
      <td>{processInfo.endTime}</td>
      <td>{vars.groupName}</td>
      <td>{vars.projectName}</td>
      <td>{vars.packageName}</td>
      <td>{vars.username}</td>
      <td>{vars.email}</td>
      <StatusItem processInfo={processInfo} />
    </tr>
  )
}

const ProcessList = observer(({ processList }) => {

  const list = processList.list
  var count = 0

  return (
    <div className='tableFixHead'>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Group</th>
            <th>Project</th>
            <th>Package</th>
            <th>User Name</th>
            <th>Email</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {list.map(t => <ProcessListItem key={t.processInstanceId} processInfo={t} count={count++} />)}
        </tbody>
      </table>
    </div>
  );
})

export default ProcessList