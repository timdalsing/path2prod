#!/usr/bin/env bash

openssl pkcs12 -export \
  -in ~/.minikube/profiles/minikube/client.crt \
  -inkey ~/.minikube/profiles/minikube/client.key \
  -name minikube_client \
  -out minikube.p12

keytool -importkeystore \
  -deststorepass changeit \
  -destkeystore minikube.jks \
  -srckeystore minikube.p12 \
  -srcstoretype PKCS12

keytool -import \
  -file ~/.minikube/ca.crt \
  -keystore minikube.jks \
  -storepass changeit \
  -alias minikube_ca