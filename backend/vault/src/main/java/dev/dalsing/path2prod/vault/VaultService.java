package dev.dalsing.path2prod.vault;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;

import java.util.Map;
import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class VaultService {

    VaultTemplate vaultTemplate;

    public void writeSecret(String path, Map<String, Object> data) {
        vaultTemplate.write("cubbyhole/" + path, data);
    }

    public void writeSecret(String path, String name, String value) {
        writeSecret(path, Map.of(name, value));
    }

    public Map<String, Object> readSecrets(String path) {
        VaultResponse response = vaultTemplate.read("cubbyhole/" + path);
        if (response == null) {
            throw new IllegalArgumentException("Secrets for path " + path + " do not exist");
        }
        return response.getData();
    }

    public Object readSecret(String path, String name) {
        Object secret = readSecrets(path).get(name);
        if (secret == null) {
            throw new IllegalArgumentException("Secret for path " + path + ", name " + name + " do not exist");
        }
        return secret;
    }

    public Object generateAndStoreCredential(String path, String name) {
        String cred = UUID.randomUUID().toString();
        writeSecret(path, name, cred);
        return cred;
    }
}
