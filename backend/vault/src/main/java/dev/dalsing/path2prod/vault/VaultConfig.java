package dev.dalsing.path2prod.vault;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultTemplate;

import static lombok.AccessLevel.PRIVATE;

@Configuration
@EnableConfigurationProperties(VaultProperties.class)
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class VaultConfig {

    VaultProperties properties;

    @Bean
    public VaultTemplate vaultTemplate() {
        VaultEndpoint endpoint = new VaultEndpoint();

        endpoint.setHost(properties.getHost());
        endpoint.setPort(properties.getPort());
        endpoint.setScheme(properties.getScheme());

        TokenAuthentication authentication = new TokenAuthentication(properties.getToken());

        return new VaultTemplate(endpoint, authentication);
    }

    @Bean
    public VaultService vaultService(VaultTemplate vaultTemplate) {
        return new VaultService(vaultTemplate);
    }
}
