package dev.dalsing.path2prod.vault;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static lombok.AccessLevel.PRIVATE;

@ConfigurationProperties("vault")
@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class VaultProperties {

    String host = "192.168.1.181";
    int port = 8200;
    String scheme = "http";
    String token = "hvs.NHPazvOBoqGhAiICOq5WdMOg";
}
