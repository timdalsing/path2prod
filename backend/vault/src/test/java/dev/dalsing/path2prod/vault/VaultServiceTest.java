package dev.dalsing.path2prod.vault;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.springframework.vault.core.VaultTemplate;

import java.util.Map;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class VaultServiceTest {

    VaultService service;
    Map<String, Object> expected = Map.of("key1", "value1");

    @BeforeEach
    void setUp() {
        VaultProperties properties = new VaultProperties();

        properties.setHost("192.168.1.181");
        properties.setPort(8200);
        properties.setToken("hvs.NHPazvOBoqGhAiICOq5WdMOg");
        properties.setScheme("http");

        VaultTemplate template = new VaultConfig(properties).vaultTemplate();
        service = new VaultService(template);
    }

    @Order(1)
    @Test
    void writeSecret() {
        service.writeSecret("test", expected);
    }

    @Order(2)
    @Test
    void readSecrets() {
        Map<String, Object> actual = service.readSecrets("test");
        Assertions.assertThat(actual).isEqualTo(expected);
    }
}