package dev.dalsing.path2prod.gitlab;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.common.Result;
import dev.dalsing.path2prod.common.Results;
import org.assertj.core.api.Assertions;
import org.flowable.engine.delegate.BpmnError;
import org.flowable.engine.delegate.DelegateExecution;
import org.gitlab4j.api.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(MockitoExtension.class)
class GitlabServiceTest {

    GitlabService service;
    static Random random = new Random();

    static String groupName = "mygroup" + random.nextInt(1000);
    static String projectName = "myproject" + random.nextInt(1000);

    @Mock
    DelegateExecution execution;
    Results results;

    @BeforeEach
    void setUp() throws Exception {
        String pat = System.getenv("GITLAB_PAT");
        GitLabApi gitLabApi = new GitLabApi("http://192.168.1.181", pat);

        ProjectApi projectApi = gitLabApi.getProjectApi();
        NamespaceApi namespaceApi = gitLabApi.getNamespaceApi();
        GroupApi groupApi = gitLabApi.getGroupApi();
        UserApi userApi = gitLabApi.getUserApi();

        results = new Results();

        when(execution.getVariable(anyString(), eq(Results.class))).thenReturn(results);
        when(execution.getVariables()).thenReturn(Map.of("results", results));

        service = new GitlabService(projectApi, namespaceApi, groupApi, userApi, new ExecutionHelper());

        service.deleteGroup(groupName, execution);
        Thread.sleep(1000L); // group isn't fully deleted immediately
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createGroup_invalidName() {
        results.clear();
        Assertions.assertThatThrownBy(() -> service.createGroup(groupName + " invalid", execution)).isInstanceOf(BpmnError.class);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isFalse();
    }

    @Test
    void createProject_invalidName() {
        createGroup();
        results.clear();
        Assertions.assertThatThrownBy(() -> service.createProject(groupName, projectName + "$invalid", execution)).isInstanceOf(BpmnError.class);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = (Results) execution.getVariables().get("results");
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isFalse();
    }

    @Test
    void createGroup() {
        results.clear();
        service.createGroup(groupName, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = (Results) execution.getVariables().get("results");
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }

    @Test
    void createProject() {
        createGroup();
        results.clear();
        service.createProject(groupName, projectName, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = (Results) execution.getVariables().get("results");
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }

    @Test
    void deleteProject() {
        createProject();
        results.clear();
        service.deleteProject(groupName, projectName, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = (Results) execution.getVariables().get("results");
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }

    @Test
    void deleteGroup() {
        createGroup();
        results.clear();
        service.deleteGroup(groupName, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = (Results) execution.getVariables().get("results");
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }
}
