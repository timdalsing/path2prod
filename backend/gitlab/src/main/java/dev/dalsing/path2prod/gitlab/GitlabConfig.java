package dev.dalsing.path2prod.gitlab;

import dev.dalsing.path2prod.common.ExecutionHelper;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static lombok.AccessLevel.PRIVATE;

@Configuration
@EnableConfigurationProperties(GitlabProperties.class)
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class GitlabConfig {

    GitlabProperties properties;

    @Bean
    public GitLabApi gitLabApi() {
        return new GitLabApi(properties.getUrl(), properties.getPat());
    }

    @Bean
    public GroupApi groupApi(GitLabApi gitLabApi) {
        return gitLabApi.getGroupApi();
    }

    @Bean
    public NamespaceApi namespaceApi(GitLabApi gitLabApi) {
        return gitLabApi.getNamespaceApi();
    }

    @Bean
    public ProjectApi projectApi(GitLabApi gitLabApi) {
        return gitLabApi.getProjectApi();
    }

    @Bean
    public RepositoryApi repositoryApi(GitLabApi gitLabApi) {
        return gitLabApi.getRepositoryApi();
    }

    @Bean
    public UserApi userApi(GitLabApi gitLabApi) {
        return gitLabApi.getUserApi();
    }

    @Bean
    public GitlabService gitlabService(ProjectApi projectApi, NamespaceApi namespaceApi, GroupApi groupApi, UserApi userApi, ExecutionHelper executionHelper) {
        return new GitlabService(projectApi, namespaceApi, groupApi, userApi, executionHelper);
    }
}
