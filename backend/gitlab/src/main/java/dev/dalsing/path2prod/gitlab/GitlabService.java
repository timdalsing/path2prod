package dev.dalsing.path2prod.gitlab;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.common.Result;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.gitlab4j.api.*;
import org.gitlab4j.api.models.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class GitlabService {

    ProjectApi projectApi;
    NamespaceApi namespaceApi;
    GroupApi groupApi;
    UserApi userApi;
    ExecutionHelper executionHelper;

    public Namespace findNamespace(String name) throws GitLabApiException {
        List<Namespace> namespaceList = namespaceApi
                .getNamespaces()
                .stream().filter(ns -> ns.getName().equals(name))
                .collect(Collectors.toList());

        if (namespaceList.size() != 1) {
            throw new IllegalArgumentException("Namespace with name " + name + " not found");
        }

        return namespaceList.get(0);
    }

    public void createGroup(String groupName, DelegateExecution execution) {
        createGroup(groupName, true, execution);
    }

    public void createGroup(String groupName, boolean ifNotExists, DelegateExecution execution) {
        log.info("createGroup: groupName = {}, ifNotExists = {}", groupName, ifNotExists);
        executionHelper.processRequest(
                () -> {
                    List<Namespace> namespaceList = namespaceApi
                            .getNamespaces()
                            .stream().filter(ns -> ns.getName().equals(groupName))
                            .collect(Collectors.toList());

                    if (namespaceList.isEmpty()) {
                        GroupParams params = new GroupParams()
                                .withName(groupName)
                                .withPath(groupName);

                        groupApi.createGroup(params);
                    }
                    return null;
                },
                "CREATE_GROUP_SUCCESS",
                (e) -> "CREATE_GROUP_ERROR",
                () -> String.format("Successfully created group: group = %s, ifNotExists = %s", groupName, ifNotExists),
                (e) -> String.format("Cannot create group: group = %s, ifNotExists = %s", groupName, ifNotExists),
                execution);
    }

    public void deleteGroup(String groupName, DelegateExecution execution) {
        log.info("deleteGroup: groupName = {}", groupName);
        executionHelper.processRequest(
                () -> {
                    Optional<Group> optionalGroup = groupApi.getGroups().stream().filter(g -> g.getName().equals(groupName)).findFirst();
                    if (optionalGroup.isEmpty()) {
                        // TODO
                    } else {
                        groupApi.deleteGroup(optionalGroup.get().getId());
                    }
                    return null;
                },
                "DELETE_GROUP_SUCCESS",
                (e) -> "DELETE_GROUP_ERROR",
                () -> String.format("Successfully deleted group: group = %s", groupName),
                (e) -> String.format("Cannot delete group: group = %s", groupName),
                execution);
    }

    public void createProject(String groupName, String projectName, DelegateExecution execution) {
        log.info("createProject: groupName = {}, projectName = {}, execution = {}", groupName, projectName, execution);
        executionHelper.processRequest(
                () -> {
                    Namespace ns = findNamespace(groupName);
                    projectApi.createProject(ns.getId(), projectName);
                    return null;
                },
                "CREATE_PROJECT_SUCCESS",
                (e) -> "CREATE_PROJECT_ERROR",
                () -> String.format("Successfully created project: group = %s, project = %s", groupName, projectName),
                (e) -> String.format("Cannot create project: group = %s, project = %s", groupName, projectName),
                execution);
    }

    public void deleteProject(String groupName, String projectName, DelegateExecution execution) {
        log.info("deleteProject: groupName = {}, projectName = {}", groupName, projectName);
        executionHelper.processRequest(
                () -> {
                    Project project = projectApi.getProject(groupName, projectName);
                    projectApi.deleteProject(project.getId());
                    return null;
                },
                "DELETE_PROJECT_SUCCESS",
                (e) -> "DELETE_PROJECT_ERROR",
                () -> String.format("Successfully deleted project: group = %s, project = %s", groupName, projectName),
                (e) -> String.format("Cannot delete project: group = %s, project = %s", groupName, projectName),
                execution);
    }

    public Result createUser(String name) throws GitLabApiException {
        User user = new User()
                .withName(name);
        String pass = ""; // TODO
        userApi.createUser(user, pass, false);
        return Result.builder().success(true).build();
    }

    public Result deleteUser(String name) throws GitLabApiException {
        User user = userApi.getUser(name);
        userApi.deleteUser(user.getId());
        return Result.builder().success(true).build();
    }

    public List<Group> getGroups() throws Exception {
        return groupApi.getGroups();
    }
}
