package dev.dalsing.path2prod.gitlab;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static lombok.AccessLevel.PRIVATE;

@ConfigurationProperties("gitlab")
@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class GitlabProperties {

    String url;
    String pat;
}
