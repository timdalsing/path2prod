package dev.dalsing.path2prod.common;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.BpmnError;
import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Supplier;

@Component
@Slf4j
public class ExecutionHelper {

    public <V> void processRequest(
            Callable<V> callable,
            String successCode,
            Function<Exception, String> errorCode,
            Supplier<String> successDescription,
            Function<Exception, String> errorDescription,
            DelegateExecution execution) {
        try {
            callable.call();
            handleSuccess(successCode, successDescription, execution);
        } catch (Exception exception) {
            handleError(exception, errorCode, errorDescription, execution);
        }
    }

    public void handleSuccess(String successCode, Supplier<String> description, DelegateExecution execution) {
        String desc = description.get();
        log.info("handleSuccess: instanceId = {}, activityId = {}, successCode = {}, desc = {}", execution.getProcessInstanceId(), 
                execution.getCurrentActivityId(), successCode, desc);

        Results results = execution.getVariable("results", Results.class);
        results.add(Result.builder()
                .success(true)
                .resultCode(successCode)
                .description(desc)
                .build());
    }

    public void handleError(Exception exception, Function<Exception, String> errorCode, Function<Exception, String> description, DelegateExecution execution) {
        String code = errorCode.apply(exception);
        String desc = description.apply(exception);
        log.error("handleError: instanceId = {}, activityId = {}, code = {}, description = {}, exception = {}", execution.getProcessInstanceId(), 
                execution.getCurrentActivityId(), code, desc, exception.toString(), exception);

        Results results = execution.getVariable("results", Results.class);
        results.add(Result.builder()
                .success(false)
                .resultCode(code)
                .description(desc)
                .build());
        throw new BpmnError(code, exception.getMessage());
    }
}
