package dev.dalsing.path2prod.common;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

import static lombok.AccessLevel.PRIVATE;

@Data
@Builder
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class Result implements Serializable {

    boolean success;

    @Builder.Default()
    String resultCode = "";

    @Builder.Default()
    String description = "";
}
