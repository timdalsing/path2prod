package dev.dalsing.path2prod.repoinit;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.common.Result;
import dev.dalsing.path2prod.common.Results;
import dev.dalsing.path2prod.gitlab.GitlabProperties;
import dev.dalsing.path2prod.vault.VaultConfig;
import dev.dalsing.path2prod.vault.VaultProperties;
import dev.dalsing.path2prod.vault.VaultService;
import freemarker.template.Configuration;
import org.flowable.engine.delegate.BpmnError;
import org.flowable.engine.delegate.DelegateExecution;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GroupApi;
import org.gitlab4j.api.ProjectApi;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.GroupParams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.vault.core.VaultTemplate;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CodeSkeletonGeneratorServiceTest {

    CodeSkeletonGeneratorService service;

    @Mock
    DelegateExecution execution;

    Results results;

    String groupName = "testgrp";
    String projectName = "testprj";
    String packageName = "com.example.myproj";
    String username = "root";
    String email = "test@test.org";
    CodeSkeletonType type = CodeSkeletonType.web;
    List<CodeSkeletonOption> options = List.of();

    @BeforeEach
    void setUp() throws Exception {
        RepoInitProperties properties = new RepoInitProperties();

        GitlabProperties gitlabProperties = new GitlabProperties();
        gitlabProperties.setUrl("http://192.168.1.181");
        gitlabProperties.setPat(System.getenv("GITLAB_PAT"));
        GitService gitService = new GitService(properties, gitlabProperties);

        GitLabApi gitLabApi = new GitLabApi(gitlabProperties.getUrl(), gitlabProperties.getPat());
        GroupApi groupApi = gitLabApi.getGroupApi();
        ProjectApi projectApi = gitLabApi.getProjectApi();
        try {
            groupApi.deleteGroup(groupName);
            Thread.sleep(5000L); // group isn't deleted immediately
        } catch (Exception e) {
            // ignore
        }
        Group group = groupApi.createGroup(new GroupParams().withName(groupName).withPath(groupName));
        projectApi.createProject(group.getId(), projectName);

        Configuration freemarkerConfig = new RepoInitFreemarkerConfig().repoInitFreemarkerConfig();
        GradleService gradleService = new GradleService(freemarkerConfig, properties);
        FileSystemService fileSystemService = new FileSystemService();
        JavaSourceService javaSourceService = new JavaSourceService(freemarkerConfig);
        PackageValidationService packageValidationService = new PackageValidationService();

        VaultProperties vaultProperties = new VaultProperties();

        vaultProperties.setHost("192.168.1.181");
        vaultProperties.setPort(8200);
        vaultProperties.setToken("hvs.NHPazvOBoqGhAiICOq5WdMOg");
        vaultProperties.setScheme("http");

        VaultTemplate template = new VaultConfig(vaultProperties).vaultTemplate();
        VaultService vaultService = new VaultService(template);

        results = new Results();
        when(execution.getVariable(anyString(), eq(Results.class))).thenReturn(results);
        when(execution.getVariables()).thenReturn(Map.of("results", results));

        service = new CodeSkeletonGeneratorService(properties, gitService, gradleService, fileSystemService, javaSourceService, packageValidationService, vaultService, new ExecutionHelper());
    }

    @Test
    void createSkeleton() {
        service.createSkeleton(groupName, projectName, packageName, username, email, type.name(), options.stream().map(Enum::name).collect(Collectors.toList()), execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }

    @Test
    void createSkeleton_groupDoesNotExist() {
        assertThatThrownBy(() -> service.createSkeleton("doesnotexist", projectName, packageName, username, email, type.name(),
                options.stream().map(Enum::name).collect(Collectors.toList()), execution)).isInstanceOf(BpmnError.class);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isFalse();
    }

    @Test
    void createSkeleton_invalidUser() {
        assertThatThrownBy(() -> service.createSkeleton(groupName, projectName, packageName, "doesnotexist", email, type.name(),
                options.stream().map(Enum::name).collect(Collectors.toList()), execution)).isInstanceOf(BpmnError.class);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isFalse();
    }
}