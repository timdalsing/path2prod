package dev.dalsing.path2prod.repoinit;

import freemarker.template.Configuration;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class GradleServiceTest {

    GradleService service;

    @BeforeEach
    void setUp() {
        Configuration cfg = new RepoInitFreemarkerConfig().repoInitFreemarkerConfig();
        RepoInitProperties properties = new RepoInitProperties();

        service = new GradleService(cfg, properties);
    }

    @Test
    void generateGradle_web_noOptions() throws Exception {
        File path = Files.createTempDirectory("test").toFile();
        service.generateGradle(path, "testgrp", "testprj", CodeSkeletonType.web, List.of());

        assertThat(path.listFiles(f -> f.getName().equals("build.gradle"))).hasSize(1);
        assertThat(path.listFiles(f -> f.getName().equals("settings.gradle"))).hasSize(1);

        File buildFile = new File(path, "build.gradle");
        assertThat(buildFile).content().contains("implementation 'org.springframework.boot:spring-boot-starter-web'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-webflux'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-stream-binder-kafka'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-stream-binder-rabbit'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-actuator'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'");
    }

    @Test
    void generateGradle_web_actuator() throws Exception {
        File path = Files.createTempDirectory("test").toFile();
        service.generateGradle(path, "testgrp", "testprj", CodeSkeletonType.web, List.of(CodeSkeletonOption.actuator));

        assertThat(path.listFiles(f -> f.getName().equals("build.gradle"))).hasSize(1);
        assertThat(path.listFiles(f -> f.getName().equals("settings.gradle"))).hasSize(1);

        File buildFile = new File(path, "build.gradle");
        assertThat(buildFile).content().contains("implementation 'org.springframework.boot:spring-boot-starter-web'");
        assertThat(buildFile).content().contains("implementation 'org.springframework.boot:spring-boot-starter-actuator'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-webflux'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-stream-binder-kafka'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-stream-binder-rabbit'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'");
    }

    @Test
    void generateGradle_webflux_actuatorSleuth() throws Exception {
        File path = Files.createTempDirectory("test").toFile();
        service.generateGradle(path, "testgrp", "testprj", CodeSkeletonType.webflux, List.of(CodeSkeletonOption.actuator,
                CodeSkeletonOption.sleuth));

        assertThat(path.listFiles(f -> f.getName().equals("build.gradle"))).hasSize(1);
        assertThat(path.listFiles(f -> f.getName().equals("settings.gradle"))).hasSize(1);

        File buildFile = new File(path, "build.gradle");
        assertThat(buildFile).content().contains("implementation 'org.springframework.boot:spring-boot-starter-webflux'");
        assertThat(buildFile).content().contains("implementation 'org.springframework.boot:spring-boot-starter-actuator'");
        assertThat(buildFile).content().contains("implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-web'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-stream-binder-kafka'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-stream-binder-rabbit'");
    }

    @Test
    void generateGradle_kafka_noOptions() throws Exception {
        File path = Files.createTempDirectory("test").toFile();
        service.generateGradle(path, "testgrp", "testprj", CodeSkeletonType.stream_kafka, List.of());

        assertThat(path.listFiles(f -> f.getName().equals("build.gradle"))).hasSize(1);
        assertThat(path.listFiles(f -> f.getName().equals("settings.gradle"))).hasSize(1);

        File buildFile = new File(path, "build.gradle");
        assertThat(buildFile).content().contains("implementation 'org.springframework.cloud:spring-cloud-stream-binder-kafka'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-webflux'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-actuator'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-web'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-stream-binder-rabbit'");
    }

    @Test
    void generateGradle_rabbit_noOptions() throws Exception {
        File path = Files.createTempDirectory("test").toFile();
        service.generateGradle(path, "testgrp", "testprj", CodeSkeletonType.stream_rabbit, List.of());

        assertThat(path.listFiles(f -> f.getName().equals("build.gradle"))).hasSize(1);
        assertThat(path.listFiles(f -> f.getName().equals("settings.gradle"))).hasSize(1);

        File buildFile = new File(path, "build.gradle");
        assertThat(buildFile).content().contains("implementation 'org.springframework.cloud:spring-cloud-stream-binder-rabbit'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-stream-binder-kafka'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-webflux'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-actuator'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'");
        assertThat(buildFile).content().doesNotContain("implementation 'org.springframework.boot:spring-boot-starter-web'");
    }

    @Test
    void generateWrapper() throws Exception {
        File path = Files.createTempDirectory("test").toFile();
        log.info("{}", path.getAbsolutePath());
        service.generateGradle(path, "testgrp", "testprj", CodeSkeletonType.web, List.of());
        service.generateWrapper(path);
        assertThat(path.listFiles(f -> f.getName().equals("gradlew"))).hasSize(1);
        assertThat(path.listFiles(f -> f.getName().equals("gradle"))).hasSize(1);
    }
}