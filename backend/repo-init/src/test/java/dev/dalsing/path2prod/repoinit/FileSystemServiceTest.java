package dev.dalsing.path2prod.repoinit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;

import static org.assertj.core.api.Assertions.assertThat;

class FileSystemServiceTest {

    FileSystemService service;

    @BeforeEach
    void setUp() {
        service = new FileSystemService();
    }

    @Test
    void createPackage() throws Exception {
        File tempDir = Files.createTempDirectory("test").toFile();
        File packageDir = service.createPackage(tempDir, "com.example.whatever");

        assertThat(packageDir).exists();
        assertThat(packageDir.getPath()).contains("com/example/whatever");
    }

    @Test
    void createDirs() throws Exception {
        File tempDir = Files.createTempDirectory("test").toFile();
        File dirs = service.createDirs(tempDir, "a/b/c");
        assertThat(dirs).exists();
        assertThat(dirs.getPath()).contains("a/b/c");
    }
}