package dev.dalsing.path2prod.repoinit;

import freemarker.template.Configuration;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class JavaSourceServiceTest {

    JavaSourceService service;

    @BeforeEach
    void setUp() {
        Configuration cfg = new RepoInitFreemarkerConfig().repoInitFreemarkerConfig();
        RepoInitProperties properties = new RepoInitProperties();

        service = new JavaSourceService(cfg);
    }

    @Test
    void createBootApp() throws Exception {
        File test = Files.createTempDirectory("test").toFile();
        String packageName = "com.example.whatever";
        String projectName = "myproj";

        File file = service.createBootApp(test, packageName, projectName);
        log.info("path = {}", file.getAbsolutePath());

        assertThat(file).exists();
        String content = Files.readString(file.toPath());
        assertThat(content).contains(packageName);
    }

    @Test
    void createBootTestApp() throws Exception {
        File test = Files.createTempDirectory("test").toFile();
        String packageName = "com.example.whatever";
        String projectName = "myproj";

        File file = service.createBootTestApp(test, packageName, projectName);
        log.info("path = {}", file.getAbsolutePath());

        assertThat(file).exists();
        String content = Files.readString(file.toPath());
        assertThat(content).contains(packageName);
    }
}