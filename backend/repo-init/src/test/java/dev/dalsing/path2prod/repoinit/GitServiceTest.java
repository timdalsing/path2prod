package dev.dalsing.path2prod.repoinit;

import dev.dalsing.path2prod.gitlab.GitlabProperties;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;


class GitServiceTest {

    GitService service;

    @BeforeEach
    void setUp() {
        RepoInitProperties properties = new RepoInitProperties();
        GitlabProperties gitlabProperties = new GitlabProperties();

        service = new GitService(properties, gitlabProperties);
    }

    @Test
    void cloneRepo() throws Exception {
        File workingDir = Files.createTempDirectory("prefix").toFile();

        String namespace = "testgrp";
        String projectName = "testprj";
        String username = "root";
        String password = "rootroot";

        service.cloneRepo(workingDir, namespace, projectName, username, password);

        String[] list = workingDir.list((dir, name) -> name.equals("README.md"));
        Assertions.assertThat(list).containsOnly("README.md");
    }

    @Test
    void addAndCommit() throws Exception {
        File workingDir = Files.createTempDirectory("prefix").toFile();

        String namespace = "testgrp";
        String projectName = "testprj";
        String username = "root";
        String password = "rootroot";
        String email = "bill@smith.org";

        service.cloneRepo(workingDir, namespace, projectName, username, password);

        File projectDir = new File(workingDir, projectName);
        Files.createFile(projectDir.toPath());

        service.addAndCommit(projectDir, namespace, projectName, username, password, email);
    }
}