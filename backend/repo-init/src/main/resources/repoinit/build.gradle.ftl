plugins {
    id 'org.springframework.boot' version '${springVersion}'
    id 'io.spring.dependency-management' version '${dependencyManagementVersion}'
    id 'java'
}

group = '${group}'
version = '${version}'

java {
    sourceCompatibility = '${jdkVersion}'
    targetCompatibility = '${jdkVersion}'
}

configurations {
    compileOnly {
        extendsFrom annotationProcessor
    }
}

repositories {
    mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom "org.springframework.cloud:spring-cloud-dependencies:${springCloudVersion}"
    }
}

dependencies {
    <#if type == 'web'>
    implementation 'org.springframework.boot:spring-boot-starter-web'
    <#elseif type == 'webflux'>
    implementation 'org.springframework.boot:spring-boot-starter-webflux'
    <#elseif type == 'stream_kafka'>
    implementation 'org.springframework.cloud:spring-cloud-stream-binder-kafka'
    <#elseif type == 'stream_rabbit'>
    implementation 'org.springframework.cloud:spring-cloud-stream-binder-rabbit'
    </#if>

    <#list options as option>
    <#if option == 'actuator'>
    implementation 'org.springframework.boot:spring-boot-starter-actuator'
    <#elseif option == 'sleuth'>
    implementation 'org.springframework.cloud:spring-cloud-starter-sleuth'
    </#if>
    </#list>

    compileOnly 'org.projectlombok:lombok'
    runtimeOnly 'org.postgresql:postgresql'
    annotationProcessor 'org.projectlombok:lombok'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
    testImplementation 'io.projectreactor:reactor-test'
}

test {
    useJUnitPlatform()
}
