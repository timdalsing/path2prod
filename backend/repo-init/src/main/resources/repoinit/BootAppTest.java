package ${packageName};

import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;

@SpringBootTest
public class ${className} {

    @Test
    public void contextLoads() {
    }
}
