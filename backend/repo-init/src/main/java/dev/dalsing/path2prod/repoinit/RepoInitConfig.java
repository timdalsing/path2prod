package dev.dalsing.path2prod.repoinit;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.gitlab.GitlabProperties;
import dev.dalsing.path2prod.vault.VaultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(RepoInitProperties.class)
@Slf4j
public class RepoInitConfig {

    @Bean
    public GitService gitService(RepoInitProperties repoInitProperties, GitlabProperties gitlabProperties) {
        return new GitService(repoInitProperties, gitlabProperties);
    }

    @Bean
    public GradleService gradleService(freemarker.template.Configuration repoInitFreemarkerConfig, RepoInitProperties repoInitProperties) {
        return new GradleService(repoInitFreemarkerConfig, repoInitProperties);
    }

    @Bean
    public FileSystemService fileSystemService() {
        return new FileSystemService();
    }

    @Bean
    public JavaSourceService javaSourceService(freemarker.template.Configuration repoInitFreemarkerConfig) {
        return new JavaSourceService(repoInitFreemarkerConfig);
    }

    @Bean
    public PackageValidationService packageValidationService() {
        return new PackageValidationService();
    }

    @Bean
    public CodeSkeletonGeneratorService codeSkeletonGeneratorService(
            RepoInitProperties repoInitProperties,
            GitService gitService,
            GradleService gradleService,
            FileSystemService fileSystemService,
            JavaSourceService javaSourceService,
            PackageValidationService packageValidationService,
            VaultService vaultService,
            ExecutionHelper executionHelper) {
        return new CodeSkeletonGeneratorService(repoInitProperties, gitService, gradleService, fileSystemService, javaSourceService,
                packageValidationService, vaultService, executionHelper);
    }
}
