package dev.dalsing.path2prod.repoinit;

import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@EnableConfigurationProperties(RepoInitProperties.class)
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class JavaSourceService {

    Configuration repoInitFreemarkerConfig;

    public JavaSourceService(Configuration repoInitFreemarkerConfig) {
        this.repoInitFreemarkerConfig = repoInitFreemarkerConfig;
    }

    public File createBootApp(File sourcePath, String packageName, String projectName) throws Exception {
        String className = getClassName(projectName);
        return createSourceFile("BootApp.java", sourcePath, packageName, className);
    }

    public File createBootTestApp(File sourcePath, String packageName, String projectName) throws Exception {
        String className = getClassName(projectName)+"Test";
        return createSourceFile("BootAppTest.java", sourcePath, packageName, className);
    }

    private String getClassName(String projectName) {
        return Character.toUpperCase(projectName.charAt(0)) + projectName.substring(1) + "App";
    }

    private File createSourceFile(String templateName, File sourcePath, String packageName, String className) throws Exception {
        String fileName = className + ".java";
        File classFile = new File(sourcePath, fileName);

        Template template = repoInitFreemarkerConfig.getTemplate(templateName);
        StringWriter writer = new StringWriter();
        Map<String, Object> variables = Map.of("packageName", packageName, "className", className);

        template.process(variables, writer);

        String content = writer.toString();

        try (FileWriter fileWriter = new FileWriter(classFile)) {
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(content);
            bufferedWriter.flush();
        }

        return classFile;
    }
}
