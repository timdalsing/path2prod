package dev.dalsing.path2prod.repoinit;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.vault.VaultService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@Component("codeSkeletonGeneratorService")
@EnableConfigurationProperties(RepoInitProperties.class)
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class CodeSkeletonGeneratorService {

    RepoInitProperties properties;
    GitService gitService;
    GradleService gradleService;
    FileSystemService fileSystemService;
    JavaSourceService javaSourceService;
    PackageValidationService packageValidationService;
    VaultService vaultService;
    ExecutionHelper executionHelper;

    public void createSkeleton(
            String groupName,
            String projectName,
            String packageName,
            String username,
            String email,
            String type,
            List<String> options,
            DelegateExecution execution) {

        executionHelper.processRequest(
                () -> {
                    packageValidationService.validatePackage(packageName);

                    Map<String, Object> secrets = vaultService.readSecrets(username + "/git");
                    String password = (String) secrets.get("password");
                    Assert.hasText(password, () -> String.format("Git password for user %s not found", username));

                    File workingDir = fileSystemService.createWorkingDir();
                    log.info("createSkeleton: workingDir = {}", workingDir);

                    log.info("createSkeleton: cloning repo: groupName = {}, projectName = {}, username = {}", groupName, projectName, username);
                    gitService.cloneRepo(workingDir, groupName, projectName, username, password);

                    log.info("createSkeleton: generating gradle: groupName = {}, projectName = {}, username = {}", groupName, projectName, username);
                    gradleService.generateGradle(workingDir, groupName, projectName, CodeSkeletonType.valueOf(type), options.stream().map(s -> CodeSkeletonOption.valueOf(s)).collect(Collectors.toList()));
                    gradleService.generateWrapper(workingDir);

                    log.info("createSkeleton: creating directories: groupName = {}, projectName = {}, username = {}", groupName, projectName, username);
                    File sourceRoot = fileSystemService.createDirs(workingDir, "src/main/java");
                    fileSystemService.createDirs(workingDir, "src/main/resources");
                    File testRoot = fileSystemService.createDirs(workingDir, "src/test/java");
                    fileSystemService.createDirs(workingDir, "src/test/resources");

                    log.info("createSkeleton: generating source files: groupName = {}, projectName = {}, username = {}", groupName, projectName, username);
                    File sourceDir = fileSystemService.createPackage(sourceRoot, packageName);
                    javaSourceService.createBootApp(sourceDir, packageName, projectName);

                    File testDir = fileSystemService.createPackage(testRoot, packageName);
                    javaSourceService.createBootTestApp(testDir, packageName, projectName);

                    log.info("createSkeleton: add and commit: groupName = {}, projectName = {}, username = {}", groupName, projectName, username);
                    gitService.addAndCommit(workingDir, groupName, projectName, username, password, email);

                    log.info("createSkeleton: complete: groupName = {}, projectName = {}, username = {}", groupName, projectName, username);
                    return null;
                },
                "REPO_INIT_SUCCESS",
                (e) -> "REPO_INIT_ERROR",
                () -> String.format("Successfully initialized repo: groupName = %s, projectName = %s, packageName = %s, username = %s, email = %s, type = %s, options = %s",
                        groupName, projectName, packageName, username, email, type, options),
                (e) -> String.format("Cannot initialize repo: groupName = %s, projectName = %s, packageName = %s, username = %s, email = %s, type = %s, options = %s, e = %s",
                        groupName, projectName, packageName, username, email, type, options, e.toString()),
                execution);
    }

}
