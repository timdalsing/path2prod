package dev.dalsing.path2prod.repoinit;

public enum CodeSkeletonType {

    web, webflux, stream_kafka, stream_rabbit
}
