package dev.dalsing.path2prod.repoinit;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static lombok.AccessLevel.PRIVATE;

@ConfigurationProperties("repo")
@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class RepoInitProperties {

    String springVersion = "2.7.1";
    String dependencyManagementVersion = "1.0.11.RELEASE";
    String defaultVersion = "0.0.1-SNAPSHOT";
    String jdkVersion = "11";
    String springCloudVersion = "2021.0.3";
}
