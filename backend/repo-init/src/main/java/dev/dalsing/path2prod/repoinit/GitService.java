package dev.dalsing.path2prod.repoinit;

import dev.dalsing.path2prod.gitlab.GitlabProperties;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.File;

import static lombok.AccessLevel.PRIVATE;

@EnableConfigurationProperties({RepoInitProperties.class, GitlabProperties.class})
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class GitService {

    RepoInitProperties properties;
    GitlabProperties gitlabProperties;

    public void cloneRepo(File workingDir, String namespace, String projectName, String username, String password) throws Exception {
        String uri = String.format("%s/%s/%s", gitlabProperties.getUrl(), namespace, projectName);

        try (Git call = Git
                     .cloneRepository()
                     .setDirectory(workingDir)
                     .setURI(uri)
                     .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password))
                .call()) {
        }
    }

    public void addAndCommit(File workingDir, String namespace, String projectName, String username, String password, String email) throws Exception {
        String uri = String.format("%s/%s/%s", gitlabProperties.getUrl(), namespace, projectName);

        try (Git open = Git.open(workingDir)) {
            open
                    .add()
                    .addFilepattern(".")
                    .call();

            open
                    .commit()
                    .setAuthor(username, email)
                    .setMessage("Initial commit")
                    .setCommitter(username, email)
                    .call();

            open
                    .push()
                    .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password))
                    .call();
        }
    }
}
