package dev.dalsing.path2prod.repoinit;

import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@EnableConfigurationProperties(RepoInitProperties.class)
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class GradleService {

    Configuration repoInitFreemarkerConfig;
    RepoInitProperties properties;

    public GradleService(Configuration repoInitFreemarkerConfig, RepoInitProperties properties) {
        this.repoInitFreemarkerConfig = repoInitFreemarkerConfig;
        this.properties = properties;
    }

    public void generateGradle(
            File path,
            String group,
            String projectName,
            CodeSkeletonType type,
            List<CodeSkeletonOption> options) throws Exception {
        String springVersion = properties.getSpringVersion();
        String dependencyManagementVersion = properties.getDependencyManagementVersion();
        String version = properties.getDefaultVersion();
        String jdkVersion = properties.getJdkVersion();
        String springCloudVersion = properties.getSpringCloudVersion();

        Map<String, Object> variables = Map.of(
                "springVersion", springVersion,
                "dependencyManagementVersion", dependencyManagementVersion,
                "group", group,
                "projectName", projectName,
                "version", version,
                "jdkVersion", jdkVersion,
                "springCloudVersion", springCloudVersion,
                "type", type,
                "options", options
        );

        generateFile("build.gradle.ftl", path, "build.gradle", variables);
        generateFile("settings.gradle.ftl", path, "settings.gradle", variables);
    }

    public void generateWrapper(File path) throws Exception {
        int exitValue = Runtime.getRuntime().exec("gradle wrapper", new String[0], path).waitFor();
        if (exitValue != 0) {
            throw new IllegalArgumentException("Gradle wrapper generation failed");
        }
    }

    private void generateFile(String templateName, File path, String fileName, Map<String, Object> variables) throws Exception {
        Template buildTemplate = repoInitFreemarkerConfig.getTemplate(templateName);
        StringWriter writer = new StringWriter();

        buildTemplate.process(variables, writer);

        String content = writer.toString();
        File buildFile = new File(path, fileName);
        try (FileWriter fileWriter = new FileWriter(buildFile)) {
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(content);
            bufferedWriter.flush();
        }
    }
}
