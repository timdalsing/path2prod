package dev.dalsing.path2prod.repoinit;

import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
public class PackageValidationService {

    Pattern pattern = Pattern.compile("(?:^\\w+|\\w+\\.\\w+)+$");

    public void validatePackage(String packageName) {
        boolean matches = pattern.matcher(packageName).matches();
        if (!matches) {
            log.error("validatePackage: invalid package: {}", packageName);
            throw new IllegalArgumentException("Invalid package: "+packageName);
        }
    }
}
