package dev.dalsing.path2prod.repoinit;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static lombok.AccessLevel.PRIVATE;

@EnableConfigurationProperties(RepoInitProperties.class)
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class FileSystemService {

    public File createPackage(File parent, String _package) {
        String[] paths = _package.split("\\.");
        File path = Paths.get(parent.getAbsolutePath(), paths).toFile();

        boolean mkdirs = path.mkdirs();
        if (!mkdirs) {
            throw new IllegalArgumentException("Cannot create dirs " + path + " in directory " + parent.getAbsolutePath());
        }

        return path;
    }

    public File createDirs(File parent, String path) {
        File file = new File(parent, path);

        boolean mkdirs = file.mkdirs();
        if (!mkdirs) {
            throw new IllegalArgumentException("Cannot create dirs " + path + " in directory " + parent.getAbsolutePath());
        }

        return file;
    }

    public File createWorkingDir() throws IOException {
        return Files.createTempDirectory("git").toFile();
    }
}
