package dev.dalsing.path2prod.orchestrator;

import dev.dalsing.path2prod.gitlab.GitlabService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.models.Group;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@RestController
@RequestMapping("/gitlab")
@CrossOrigin("http://localhost:3000")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class GitlabController {

    GitlabService gitlabService;

    @GetMapping("/groupnames")
    @ResponseBody
    public List<String> getGroupNames() throws Exception {
        return gitlabService
                .getGroups()
                .stream()
                .map(Group::getName)
                .collect(Collectors.toList());
    }
}
