package dev.dalsing.path2prod.orchestrator;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.gitlab.GitlabConfig;
import dev.dalsing.path2prod.kubernetes.KubernetesConfig;
import dev.dalsing.path2prod.kubernetes.KubernetesFreemarkerConfig;
import dev.dalsing.path2prod.kubernetes.KubernetesRestTemplateConfig;
import dev.dalsing.path2prod.repoinit.GitService;
import dev.dalsing.path2prod.repoinit.RepoInitConfig;
import dev.dalsing.path2prod.repoinit.RepoInitFreemarkerConfig;
import dev.dalsing.path2prod.vault.VaultConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
        ExecutionHelper.class,
        GitlabConfig.class,
        GitService.class,
        KubernetesRestTemplateConfig.class,
        KubernetesFreemarkerConfig.class,
        KubernetesConfig.class,
        RepoInitConfig.class,
        RepoInitFreemarkerConfig.class,
        VaultConfig.class
})
public class OrchestratorApp {

    public static void main(String[] args) {
        SpringApplication.run(OrchestratorApp.class, args);
    }
}
