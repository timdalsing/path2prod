package dev.dalsing.path2prod.orchestrator;

import dev.dalsing.path2prod.common.Result;
import dev.dalsing.path2prod.common.Results;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.web.bind.annotation.*;

import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@RestController
@CrossOrigin("http://localhost:3000")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ProcessController {

    RuntimeService runtimeService;
    HistoryService historyService;

    @GetMapping("/process/{username}")
    public List<ProcessInfo> getProcesses(@PathVariable("username") String username) {
        return historyService
                .createHistoricProcessInstanceQuery()
                .processDefinitionKey("createProject")
                .includeProcessVariables()
                .variableValueEquals("username", username)
                .finished()
                .orderByProcessInstanceEndTime().desc()
                .list()
                .stream()
                .map(hist -> {
                    Results results = (Results) hist.getProcessVariables().get("results");
                    List<Result> errorResults = results.stream().filter(r -> !r.isSuccess()).collect(Collectors.toList());
                    String status = errorResults.isEmpty() ? "SUCCESS" : "ERROR";
                    Result result = errorResults.isEmpty() ? null : errorResults.get(0);
                    return ProcessInfo.builder()
                            .processInstanceId(hist.getId())
                            .processDefinitionId(hist.getProcessDefinitionId())
                            .processDefinitionKey(hist.getProcessDefinitionKey())
                            .processDefinitionName(hist.getProcessDefinitionName())
                            .processDefinitionVersion(hist.getProcessDefinitionVersion())
                            .startActivityId(hist.getStartActivityId())
                            .endActivityId(hist.getEndActivityId())
                            .processVariables(hist.getProcessVariables())
                            .startTime(hist.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                            .endTime(hist.getEndTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                            .status(status)
                            .resultCode(result == null ? "" : result.getResultCode())
                            .resultDescription(result == null ? "" : result.getDescription())
                            .build();
                })
                .collect(Collectors.toList());
    }

    @PostMapping("/process")
    public @ResponseBody ProcessInfo createProcess(@RequestBody Map<String, Object> variables) {
        log.info("createProcess: variables = {}", variables);

        variables.put("results", new Results());

        ProcessInstance processInstance = runtimeService
                .createProcessInstanceBuilder()
                .processDefinitionKey("createProject")
                .variables(variables)
                .startAsync();

        return ProcessInfo.builder()
                .processInstanceId(processInstance.getId())
                .processDefinitionId(processInstance.getProcessDefinitionId())
                .processDefinitionKey(processInstance.getProcessDefinitionKey())
                .processDefinitionName(processInstance.getProcessDefinitionName())
                .processDefinitionVersion(processInstance.getProcessDefinitionVersion())
                .processVariables(variables)
                .startTime(processInstance.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                .build();
    }
}
