package dev.dalsing.path2prod.orchestrator;

import dev.dalsing.path2prod.kubernetes.NamespaceService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@RestController
@RequestMapping("/kubernetes")
@CrossOrigin("http://localhost:3000")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class KubernetesController {

    NamespaceService namespaceService;

    @GetMapping("/namespace")
    @ResponseBody
    public List<String> getNamespaces() {
        return namespaceService.getNamespaces();
    }
}
