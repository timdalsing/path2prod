package dev.dalsing.path2prod.orchestrator;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = PRIVATE)
public class ProcessInfo {

    String processInstanceId;
    String processDefinitionId;
    String processDefinitionKey;
    String processDefinitionName;
    int processDefinitionVersion;
    String startActivityId;
    String endActivityId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime endTime;
    Map<String, Object> processVariables;

    String status;
    String resultCode;
    String resultDescription;
}
