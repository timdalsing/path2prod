package dev.dalsing.path2prod.orchestrator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GitlabControllerTest {

    @Autowired
    GitlabController controller;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getGroupNames() throws Exception {
        List<String> groupNames = controller.getGroupNames();
        assertThat(groupNames).isNotEmpty();
    }
}