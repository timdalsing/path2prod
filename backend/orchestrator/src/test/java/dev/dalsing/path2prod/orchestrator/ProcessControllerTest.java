package dev.dalsing.path2prod.orchestrator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@Slf4j
class ProcessControllerTest {

    @Autowired
    ProcessController controller;

    @Autowired
    HistoryService historyService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getProcesses() throws Exception {
        List<ProcessInfo> processes = controller.getProcesses("root");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        log.info("getProcesses: processes = {}", objectMapper.writeValueAsString(processes));
        assertThat(processes).isNotEmpty();
    }

    @Test
    void createProcess() {
    }

//    @Test
    void taskHist() throws Exception {
        List<HistoricProcessInstance> instances = historyService.createHistoricProcessInstanceQuery().orderByProcessInstanceStartTime().desc().list();
        HistoricProcessInstance instance = instances.get(1);
        Map<String, Object> variables = instance.getProcessVariables();
        String id = instance.getId();

        List<HistoricActivityInstance> list = historyService
                .createHistoricActivityInstanceQuery()
                .processInstanceId(id)
//                .activityType("serviceTask")
                .orderByHistoricActivityInstanceStartTime()
                .asc()
                .list();

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String json = mapper.writeValueAsString(list);
        log.info(json);
    }
}