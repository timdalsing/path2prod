package dev.dalsing.path2prod.orchestrator;

import dev.dalsing.path2prod.common.Results;
import dev.dalsing.path2prod.common.ServiceType;
import dev.dalsing.path2prod.gitlab.GitlabService;
import dev.dalsing.path2prod.kubernetes.NamespaceService;
import dev.dalsing.path2prod.repoinit.CodeSkeletonType;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.*;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Slf4j
class OrchestratorAppTest {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    ProcessEngine processEngine;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    HistoryService historyService;

    @Autowired
    TaskService taskService;

    @Autowired
    GitlabService gitlabService;

    @Autowired
    NamespaceService namespaceService;

    String groupName = "testgrp";
    String projectName = "testprj";
    String packageName = "com.example.test";
    String username = "root";
    String password = "rootroot";
    String email = "test@test.org";

    @BeforeEach
    void setUp() throws Exception {
        log.info("setUp: deleting group {}", groupName);
        DelegateExecution execution = Mockito.mock(DelegateExecution.class);
        Mockito.when(execution.getVariables()).thenReturn(Map.of("results", new Results()));
        gitlabService.deleteGroup(groupName, execution);

        log.info("setUp: deleting namespace {}", projectName);
        namespaceService.deleteNamespace(projectName, execution);

        Thread.sleep(10000L); // wait for gitlab to delete group
    }

    @Test
    void execProcess_success_web_noOptions_noServices() throws Exception {
        Map<String, Object> variables = Map.of(
                "groupName", groupName,
                "projectName", projectName,
                "packageName", packageName,
                "username", username,
                "password", password,
                "email", email,
                "type", CodeSkeletonType.web,
                "options", List.of(),
                "services", Set.of()
        );
        log.info("execProcess");
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("createProject", variables);
    }

    @Test
    void execProcess_success_web_noOptions_withPostgres() throws Exception {
        Map<String, Object> variables = Map.of(
                "groupName", groupName,
                "projectName", projectName,
                "packageName", packageName,
                "username", username,
                "password", password,
                "email", email,
                "type", CodeSkeletonType.web,
                "options", List.of(),
                "services", Set.of(ServiceType.postgresql)
        );
        log.info("execProcess");
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("createProject", variables);
    }

    @Test
    void execProcess_badNS() {
        Map<String, Object> variables = Map.of(
                "groupName", groupName,
                "projectName", "test prj",
                "packageName", packageName,
                "username", username,
                "password", password,
                "email", email
        );
        log.info("execProcess");
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("createProject", variables);
    }

    @Test
    void execProcess_badPackage() {
        Map<String, Object> variables = Map.of(
                "groupName", groupName,
                "projectName", projectName,
                "packageName", "com.example test",
                "username", username,
                "password", password,
                "email", email
        );
        log.info("execProcess");
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("createProject", variables);
    }

}