package dev.dalsing.path2prod.kubernetes;

import freemarker.template.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;


class ServicesServiceTest {

    ServicesService service;

    @BeforeEach
    void setUp() throws Exception {
        KubernetesProperties kubernetesProperties = new KubernetesProperties();
        RestTemplate restTemplate = new KubernetesRestTemplateConfig(kubernetesProperties).kubernetesRestTemplate();
        Configuration cfg = new KubernetesFreemarkerConfig().kubernetesFreemarkerConfig();

        ApplyService applyService = new ApplyService(restTemplate, cfg);
        service = new ServicesService(applyService);

        try {
            service.deleteService("default", "postgresql-service");
        } catch (Exception e) {
            // ignore
        }
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createService() throws Exception {
        service.createService("default", "postgresql-service", "postgres", "None");
    }
}