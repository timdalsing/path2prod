package dev.dalsing.path2prod.kubernetes;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.common.Result;
import dev.dalsing.path2prod.common.Results;
import freemarker.template.Configuration;
import org.flowable.engine.delegate.BpmnError;
import org.flowable.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NamespaceServiceTest {

    NamespaceService service;

    @Mock
    DelegateExecution execution;

    Results results;

    static Random random = new Random();
    String namespace = "mynamespace" + random.nextInt(1000);

    @BeforeEach
    void setUp() throws Exception {
        KubernetesProperties kubernetesProperties = new KubernetesProperties();
        kubernetesProperties.setRootUrl("https://192.168.1.181:8443");
        kubernetesProperties.setKeystorePath("minikube.jks");
        kubernetesProperties.setKeystorePassword("changeit");

        RestTemplate restTemplate = new KubernetesRestTemplateConfig(kubernetesProperties).kubernetesRestTemplate();
        Configuration cfg = new KubernetesFreemarkerConfig().kubernetesFreemarkerConfig();

        ApplyService applyService = new ApplyService(restTemplate, cfg);
        ObjectMapper objectMapper = new ObjectMapper();
        results = new Results();
        when(execution.getVariable(anyString(), eq(Results.class))).thenReturn(results);
        when(execution.getVariables()).thenReturn(Map.of("results", results));
        service = new NamespaceService(applyService, objectMapper, new ExecutionHelper());
        try {
            service.deleteNamespace(namespace, execution);
        } catch (Exception e) {
            // ignore
        }
        Thread.sleep(3000L); // wait for delete to complete
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createNamespace() {
        results.clear();
        service.createNamespace(namespace, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }

    @Test
    void createNamespace_invalidName() {
        results.clear();
        assertThatThrownBy(() -> service.createNamespace("invalid+namespace", execution)).isInstanceOf(BpmnError.class);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isFalse();
    }

    @Test
    void deleteNamespace() {
        service.createNamespace(namespace, execution);
        results.clear();
        service.deleteNamespace(namespace, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }
}