package dev.dalsing.path2prod.kubernetes;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.common.Result;
import dev.dalsing.path2prod.common.Results;
import freemarker.template.Configuration;
import org.flowable.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PersistentVolumeServiceTest {

    PersistentVolumeService service;

    @Mock
    DelegateExecution execution;

    Results results;

    static Random random = new Random();
    static String namespace = "default";
    static String size = "1Gi";
    static String storageClass = "standard";
    static String pvName = "mypv" + random.nextInt(1000);
    static String pvcName = "mypvc" + random.nextInt(1000);

    @BeforeEach
    void setUp() throws Exception {
        KubernetesProperties kubernetesProperties = new KubernetesProperties();
        kubernetesProperties.setRootUrl("https://192.168.1.181:8443");
        kubernetesProperties.setKeystorePath("minikube.jks");
        kubernetesProperties.setKeystorePassword("changeit");

        RestTemplate restTemplate = new KubernetesRestTemplateConfig(kubernetesProperties).kubernetesRestTemplate();
        Configuration cfg = new KubernetesFreemarkerConfig().kubernetesFreemarkerConfig();

        ApplyService applyService = new ApplyService(restTemplate, cfg);

        results = new Results();
        when(execution.getVariable(anyString(), eq(Results.class))).thenReturn(results);
        when(execution.getVariables()).thenReturn(Map.of("results", results));

        service = new PersistentVolumeService(applyService, new ExecutionHelper());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createVolume() {
        try {
            service.deleteVolume(pvName, execution);
        } catch (Exception e) {
            // ignore
        }
        results.clear();
        service.createVolume(pvName, size, storageClass, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }

    @Test
    void deleteVolume() {
        createVolume();
        results.clear();
        service.deleteVolume(pvName, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }

    @Test
    void createVolumeClaim() {
        results.clear();
        service.createVolumeClaim(namespace, pvcName, size, storageClass, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }

    @Test
    void deleteVolumeClaim() {
        createVolumeClaim();
        results.clear();
        service.deleteVolumeClaim(namespace, pvcName, execution);
        assertThat(execution.getVariables()).containsKey("results");
        Results results = execution.getVariable("results", Results.class);
        assertThat(results).hasSize(1);
        Result actual = results.get(0);
        assertThat(actual.isSuccess()).isTrue();
    }
}