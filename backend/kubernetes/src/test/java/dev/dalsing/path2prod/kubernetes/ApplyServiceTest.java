package dev.dalsing.path2prod.kubernetes;

import freemarker.template.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.util.Map;


class ApplyServiceTest {

    ApplyService service;

    @BeforeEach
    void setUp() throws Exception {
        KubernetesProperties kubernetesProperties = new KubernetesProperties();
        RestTemplate restTemplate = new KubernetesRestTemplateConfig(kubernetesProperties).kubernetesRestTemplate();
        Configuration cfg = new KubernetesFreemarkerConfig().kubernetesFreemarkerConfig();

        service = new ApplyService(restTemplate, cfg);

        try {
            service.delete("/v1/namespaces/{namespace}/services/{name}", Map.of("namespace", "default", "name", "postgresql"));
        } catch (Exception e) {
            // ignore
        }
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void apply() throws Exception {
        service.apply("service.yml",
                "/v1/namespaces/{namespace}/services",
                Map.of("namespace","default","name", "postgresql", "app", "postgresql", "clusterIP", "None"));
    }
}