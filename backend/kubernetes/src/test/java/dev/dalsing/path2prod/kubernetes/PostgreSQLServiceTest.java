package dev.dalsing.path2prod.kubernetes;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.common.Results;
import dev.dalsing.path2prod.vault.VaultConfig;
import dev.dalsing.path2prod.vault.VaultProperties;
import dev.dalsing.path2prod.vault.VaultService;
import freemarker.template.Configuration;
import org.flowable.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PostgreSQLServiceTest {

    PostgreSQLService service;

    @Mock
    DelegateExecution execution;

    Results results;

    String namespace = "default";
    String name = "mydb";
    String username = "root";

    @BeforeEach
    void setUp() throws Exception {
        KubernetesProperties kubernetesProperties = new KubernetesProperties();
        kubernetesProperties.setRootUrl("https://192.168.1.181:8443");
        kubernetesProperties.setKeystorePath("minikube.jks");
        kubernetesProperties.setKeystorePassword("changeit");

        RestTemplate restTemplate = new KubernetesRestTemplateConfig(kubernetesProperties).kubernetesRestTemplate();
        Configuration cfg = new KubernetesFreemarkerConfig().kubernetesFreemarkerConfig();

        ApplyService applyService = new ApplyService(restTemplate, cfg);

        VaultProperties properties = new VaultProperties();

        properties.setHost("192.168.1.181");
        properties.setPort(8200);
        properties.setToken("hvs.NHPazvOBoqGhAiICOq5WdMOg");
        properties.setScheme("http");

        VaultTemplate template = new VaultConfig(properties).vaultTemplate();
        VaultService vaultService = new VaultService(template);

        results = new Results();

        when(execution.getVariable(anyString(), eq(Results.class))).thenReturn(results);

        service = new PostgreSQLService(applyService, vaultService, new ExecutionHelper());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createPostgreSQLDatabase() {
        try {
            service.deletePostgreSQLDatabase(namespace, name, execution);
        } catch (Exception e) {
            // ignore
        }
        service.createPostgreSQLDatabase(namespace, name,5432, username, execution);
    }

    @Test
    void deletePostgreSQLDatabase() {
        try {
            service.createPostgreSQLDatabase(namespace, name,5432, username, execution);
        } catch (Exception e) {
            // ignore
        }
        service.deletePostgreSQLDatabase(namespace, name, execution);
    }

    @Test
    void createPostgreSQLService() {
        try {
            service.deletePostgreSQLService(namespace, name, execution);
        }
        catch (Exception e) {
            // ignore
        }
        results.clear();
        service.createPostgreSQLService(namespace, name, execution);
    }

    @Test
    void deletePostgreSQLService() {
        try {
            service.createPostgreSQLService(namespace, name, execution);
        }
        catch (Exception e) {
            // ignore
        }
        results.clear();
        service.deletePostgreSQLService(namespace, name, execution);
    }
}