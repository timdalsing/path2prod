package dev.dalsing.path2prod.kubernetes;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ServicesService {

    ApplyService applyService;

    public void createService(String namespace, String serviceName, String app, String clusterIP) throws Exception {
        Map<String, Object> variables = Map
                .of("namespace", namespace,
                        "name", serviceName,
                        "app", app,
                        "clusterIP", clusterIP);
        applyService.apply("service.yml", "/api/v1/namespaces/{namespace}/services", variables);
    }

    public void deleteService(String namespace, String serviceName) throws Exception {
        Map<String, Object> variables = Map
                .of("namespace", namespace,
                        "name", serviceName);
        applyService.delete("/api/v1/namespaces/{namespace}/services/{name}", variables);
    }
}
