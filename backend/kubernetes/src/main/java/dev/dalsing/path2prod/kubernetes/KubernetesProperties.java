package dev.dalsing.path2prod.kubernetes;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static lombok.AccessLevel.PRIVATE;

@ConfigurationProperties("k8s")
@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class KubernetesProperties {

    String rootUrl;
    String keystorePath;
    String keystorePassword;
}
