package dev.dalsing.path2prod.kubernetes;

import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.vault.VaultService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;

import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class PostgreSQLService {

    ApplyService applyService;
    VaultService vaultService;
    ExecutionHelper executionHelper;

    public void createPostgreSQLService(String namespace, String name, DelegateExecution execution) {
        log.info("createPostgreSQLService: namespace = {}, name = {}", namespace, name);
        executionHelper.processRequest(
                () -> {
                    applyService.apply("service.yml"
                            , "/api/v1/namespaces/{namespace}/services"
                            , Map.of("namespace", namespace,
                                    "name", "postgresql-"+name,
                                    "app", "postgresql-"+name,
                                    "clusterIP", "None"));
                    return null;
                },
                "CREATE_POSTGRESQL_SERVICE_SUCCESS",
                (e) -> "CREATE_POSTGRESQL_SERVICE_ERROR",
                () -> String.format("Successfully created PostgreSQL service: namespace = %s, name = %s", namespace, name),
                (e) -> String.format("Cannot create PostgreSQL service: namespace = %s, name = %s", namespace, name),
                execution);
    }

    public void deletePostgreSQLService(String namespace, String name, DelegateExecution execution) {
        log.info("deletePostgreSQLService: namespace = {}, name = {}", namespace, name);
        executionHelper.processRequest(
                () -> {
                    Map<String, Object> variables = Map.of("namespace", namespace, "name", "postgresql-"+name);
                    applyService.delete("/api/v1/namespaces/{namespace}/services/{name}", variables);
                    return null;
                },
                "DELETE_POSTGRESQL_SERVICE_SUCCESS",
                (e) -> "DELETE_POSTGRESQL_SERVICE_ERROR",
                () -> String.format("Successfully deleted PostgreSQL service: namespace = %s, name = %s", namespace, name),
                (e) -> String.format("Cannot delete PostgreSQL service: namespace = %s, name = %s", namespace, name),
                execution);
    }

    public void createPostgreSQLDatabase(String namespace, String name, int port, String username, DelegateExecution execution) {
        log.info("createPostgreSQLDatabase: namespace = {}, name = {}, port = {}, username = {}", namespace, name, port, username);
        executionHelper.processRequest(
                () -> {
                    String password = (String) vaultService.readSecret(username + "/git", "password");
                    applyService.apply("postgresql.yml"
                            , "/apis/apps/v1/namespaces/{namespace}/statefulsets"
                            , Map.of("namespace", namespace,
                                    "name", name,
                                    "image", "postgres:14",
                                    "username", username,
                                    "password", password,
                                    "port", port,
                                    "volumeSize", "1Gi"));
                    return null;
                },
                "CREATE_POSTGRESQL_DATABASE_SUCCESS",
                (e) -> "CREATE_POSTGRESQL_DATABASE_ERROR",
                () -> String.format("Successfully created PostgreSQL database: namespace = %s, name = %s, port = %s, username = %s", namespace, name, port, username),
                (e) -> String.format("Cannot create PostgreSQL database: namespace = %s, name = %s, port = %s, username = %s", namespace, name, port, username),
                execution);
    }

    public void deletePostgreSQLDatabase(String namespace, String name, DelegateExecution execution) {
        log.info("deletePostgreSQLDatabase: namespace = {}, name = {}", namespace, name);
        executionHelper.processRequest(
                () -> {
                    Map<String, Object> variables = Map.of("namespace", namespace, "name", name);
                    applyService.delete("/apis/apps/v1/namespaces/{namespace}/statefulsets/postgresql-{name}", variables);
                    return null;
                },
                "DELETE_POSTGRESQL_DATABASE_SUCCESS",
                (e) -> "DELETE_POSTGRESQL_DATABASE_ERROR",
                () -> String.format("Successfully deleted PostgreSQL database: namespace = %s, name = %s", namespace, name),
                (e) -> String.format("Cannot delete PostgreSQL database: namespace = %s, name = %s", namespace, name),
                execution);
    }
}
