package dev.dalsing.path2prod.kubernetes;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.dalsing.path2prod.common.ExecutionHelper;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NamespaceService {

    ApplyService applyService;
    ObjectMapper objectMapper;
    ExecutionHelper executionHelper;

    public void createNamespace(String namespace, DelegateExecution execution) {
        createNamespace(namespace, true, execution);
    }

    public void createNamespace(String namespace, boolean ifNotExists, DelegateExecution execution) {
        log.info("createNamespace: namespace = {}", namespace);

        if (ifNotExists && namespaceExists(namespace)) {
            executionHelper.handleSuccess(
                    "CREATE_NAMESPACE_SUCCESS",
                    () -> String.format("Namespace already exists: namespace = %s, ifNotExists = true", namespace),
                    execution);
        } else {
            executionHelper.processRequest(
                    () -> {
                        Map<String, Object> variables = Map.of("name", namespace);
                        applyService.apply("namespace.yml", "/api/v1/namespaces", variables);
                        return null;
                    },
                    "CREATE_NAMESPACE_SUCCESS",
                    (e) -> "CREATE_NAMESPACE_ERROR",
                    () -> String.format("Successfully created namespace: namespace = %s", namespace),
                    (e) -> String.format("Cannot create namespace: namespace = %s", namespace),
                    execution);
        }
    }

    public void deleteNamespace(String namespace, DelegateExecution execution) {
        log.info("deleteNamespace: namespace = {}", namespace);
        executionHelper.processRequest(
                () -> {
                    Map<String, Object> variables = Map.of("name", namespace);
                    applyService.delete("/api/v1/namespaces/{name}", variables);
                    return null;
                },
                "DELETE_NAMESPACE_SUCCESS",
                (e) -> "DELETE_NAMESPACE_ERROR",
                () -> String.format("Successfully deleted namespace: namespace = %s", namespace),
                (e) -> String.format("Cannot delete namespace: namespace = %s", namespace),
                execution);
    }

    public boolean namespaceExists(String namespace) {
        try {
            applyService.get("/api/v1/namespaces/{namespace}", Map.of("namespace", namespace));
            return true;
        } catch (HttpClientErrorException.NotFound e) {
            return false;
        } catch (Exception e) {
            throw new IllegalArgumentException(e.toString(), e);
        }
    }

    private Map<String, Object> parseResponse(byte[] data) {
        try {
            return objectMapper.readValue(data, Map.class);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.toString(), e);
        }
    }

    public List<String> getNamespaces() {
        try {
            Map<String, Object> response = applyService.get("/api/v1/namespaces/");
            List<Map<String, Object>> items = (List<Map<String, Object>>) response.get("items");
            return items
                    .stream()
                    .map(item -> {
                        Map<String, Object> metadata = (Map<String, Object>) item.get("metadata");
                        return (String) metadata.get("name");
                    })
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new IllegalArgumentException(e.toString(), e);
        }
    }
}
