package dev.dalsing.path2prod.kubernetes;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.X509Certificate;

import static lombok.AccessLevel.PRIVATE;

@Configuration
@EnableConfigurationProperties(KubernetesProperties.class)
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class KubernetesRestTemplateConfig {

    KubernetesProperties properties;

    @Bean
    public RestTemplate kubernetesRestTemplate() throws Exception {
        final char[] pass = properties.getKeystorePassword().toCharArray();
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        InputStream inputStream = new ClassPathResource(properties.getKeystorePath()).getInputStream();
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(inputStream, pass);
        SSLContext sslContext = SSLContextBuilder
                .create()
                .loadKeyMaterial(keyStore, pass)
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

        HttpClient client = HttpClients.custom()
                .setSSLContext(sslContext)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(client);

        return new RestTemplateBuilder()
                .rootUri(properties.getRootUrl())
                .requestFactory(() -> requestFactory)
                .build();
    }
}
