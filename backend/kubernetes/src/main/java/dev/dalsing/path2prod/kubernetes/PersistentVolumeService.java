package dev.dalsing.path2prod.kubernetes;

import dev.dalsing.path2prod.common.ExecutionHelper;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;

import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class PersistentVolumeService {

    ApplyService applyService;
    ExecutionHelper executionHelper;

    public void createVolume(String name, String size, String storageClass, DelegateExecution execution) {
        log.info("createVolume: name = {}, size = {}, storageClass = {}", name, size, storageClass);
        executionHelper.processRequest(
                () -> {
                    String url = "/api/v1/persistentvolumes";
                    Map<String, Object> vars = Map.of("name", name, "size", size, "storageClass", storageClass);
                    applyService.apply("pv.yml", url, vars);
                    return null;
                },
                "CREATE_VOLUME_SUCCESS",
                (e) -> "CREATE_VOLUME_ERROR",
                () -> String.format("Successfully created volume: name = %s, size = %s, storageClass = %s", name, size, storageClass),
                (e) -> String.format("Cannot create volume: name = %s, size = %s, storageClass = %s", name, size, storageClass),
                execution);
    }

    public void deleteVolume(String name, DelegateExecution execution) {
        log.info("deleteVolume: name = {}", name);
        executionHelper.processRequest(
                () -> {
                    String url = "/api/v1/persistentvolumes/{name}";
                    Map<String, Object> vars = Map.of("name", name);
                    applyService.delete(url, vars);
                    return null;
                },
                "DELETE_VOLUME_SUCCESS",
                (e) -> "DELETE_VOLUME_ERROR",
                () -> String.format("Successfully deleted volume: name = %s", name),
                (e) -> String.format("Cannot delete volume: name = %s", name),
                execution);
    }

    public void createVolumeClaim(String namespace, String name, String size, String storageClass, DelegateExecution execution) {
        log.info("createVolumeClaim: namespace = {}, name = {}, size = {}, storageClass = {}", namespace, name, size, storageClass);
        executionHelper.processRequest(
                () -> {
                    String url = "/api/v1/namespaces/{namespace}/persistentvolumeclaims";
                    Map<String, Object> vars = Map.of("namespace", namespace, "name", name, "size", size, "storageClass", storageClass);
                    applyService.apply("pvc.yml", url, vars);
                    return null;
                },
                "CREATE_VOLUMECLAIM_SUCCESS",
                (e) -> "CREATE_VOLUMECLAIM_ERROR",
                () -> String.format("Successfully created volume claim: name = %s, size = %s, storageClass = %s", name, size, storageClass),
                (e) -> String.format("Cannot create volume claim: name = %s, size = %s, storageClass = %s", name, size, storageClass),
                execution);
    }

    public void deleteVolumeClaim(String namespace, String name, DelegateExecution execution) {
        log.info("deleteVolumeClaim: namespace = {}, name = {}", namespace, name);
        executionHelper.processRequest(
                () -> {
                    String url = "/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}";
                    Map<String, Object> vars = Map.of("namespace", namespace, "name", name);
                    applyService.delete(url, vars);
                    return null;
                },
                "DELETE_VOLUMECLAIM_SUCCESS",
                (e) -> "DELETE_VOLUMECLAIM_ERROR",
                () -> String.format("Successfully deleted volume claim: name = %s", name),
                (e) -> String.format("Cannot delete volume claim: name = %s", name),
                execution);
    }
}
