package dev.dalsing.path2prod.kubernetes;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.dalsing.path2prod.common.ExecutionHelper;
import dev.dalsing.path2prod.vault.VaultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@Slf4j
public class KubernetesConfig {

    @Bean
    public ApplyService applyService(RestTemplate kubernetesRestTemplate, freemarker.template.Configuration kubernetesFreemarkerConfig) {
        return new ApplyService(kubernetesRestTemplate, kubernetesFreemarkerConfig);
    }

    @Bean
    public NamespaceService namespaceService(ApplyService applyService, ObjectMapper objectMapper, ExecutionHelper executionHelper) {
        return new NamespaceService(applyService, objectMapper, executionHelper);
    }

    @Bean
    public PersistentVolumeService persistentVolumeService(ApplyService applyService, ExecutionHelper executionHelper) {
        return new PersistentVolumeService(applyService, executionHelper);
    }

    @Bean
    public PostgreSQLService postgreSQLService(ApplyService applyService, VaultService vaultService, ExecutionHelper executionHelper) {
        return new PostgreSQLService(applyService, vaultService, executionHelper);
    }

    @Bean
    public ServicesService service(ApplyService applyService) {
        return new ServicesService(applyService);
    }
}
