package dev.dalsing.path2prod.kubernetes;

import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;
import org.yaml.snakeyaml.Yaml;

import java.io.StringWriter;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ApplyService {

    RestTemplate kubernetesRestTemplate;
    Configuration kubernetesFreemarkerConfig;

    Yaml yaml = new Yaml();

    public ApplyService(RestTemplate kubernetesRestTemplate, Configuration kubernetesFreemarkerConfig) {
        this.kubernetesRestTemplate = kubernetesRestTemplate;
        this.kubernetesFreemarkerConfig = kubernetesFreemarkerConfig;
    }

    public Map<String, Object> get(String url, Map<String, Object> variables) throws Exception {
        return kubernetesRestTemplate.getForObject(url, Map.class, variables);
    }

    public Map<String, Object> get(String url) throws Exception {
        return kubernetesRestTemplate.getForObject(url, Map.class);
    }

    public void apply( String templateName, String url, Map<String, Object> variables) throws Exception {
        Template template = kubernetesFreemarkerConfig.getTemplate(templateName);
        StringWriter writer = new StringWriter();
        template.process(variables, writer);
        String processedTemplate = writer.toString();
        Map<String, Object> map = this.yaml.load(processedTemplate);

        kubernetesRestTemplate.postForObject(url, map, Void.class, variables);
    }

    public void delete(String url, Map<String, Object> variables) {
        kubernetesRestTemplate.delete(url, variables);
    }
}
