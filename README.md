# Path 2 Prod Demo

This repo contains a demo of a path-2-prod automation tool that quickly creates and generates the basic artifacts required to create a new application.  This system automates the many tasks that are required for developing new components.  For example:

- Create Git repo
- Create database instance and service in Kubernetes
- Generate application skeleton, including build artifacts and basic application classes

In a typical development environment, it can take many weeks or even months to accomplish many of these tasks since the normal process is to use JIRA tickets that are worked by different departments.  This is especially true in large organizations since they are often rigidly siloed.

By automating these steps using a self-service UI and backend the need for JIRA tickets and other inefficient mechanisms can be avoided.  This is usually preferrable to providing developers direct access to systems owned by other parts of the organization, which can be a security issue.  This tool also ensures specific rules and conventions are followed.

This system consists of 2 major components:

- Backend application that implements the workflow
- Frontend web app accessible by developers

## Backend

The backend is a Spring Boot application that uses the [Flowable](https://flowable.com) workflow engine.  Workflows are written in [BPMN2](https://www.bpmn.org/) business process management language.  BPMN2 includes a compensation mechanism that can be used to backout any changes when an error occurs, similar to a database transaction.

Plugins are used to perform integrations with external systems.  The current integrations are implemented:

- [Gitlab](https://about.gitlab.com/).  The REST API is used via a Java client library to allow the programmitic creation of groups and projects.  OSS Gitlab CE is used in this demo.
- [Kubernetes](https://kubernetes.io/).  The REST API is used to manage Kubernetes objects via the api server.  Any Kubernetes distribution can be used.  [Minikube](https://minikube.sigs.k8s.io/docs/) using the `none` (bare metal) driver is used.
- [Hashicorp Vault](https://www.hashicorp.com/products/vault) is used for credentials management, including the storage of Git and login credentials.  [Spring's Vault](https://spring.io/projects/spring-vault) module is used for access.

Flowable supports Spring beans as Service Tasks.  This is used for all integrations between the BPMN2 workflows and the plugins.

Also, a plugin is included that uses [Freemarker](https://freemarker.apache.org/) templates and [JGit](https://www.eclipse.org/jgit/) for generating skeleton artifacts and committing them to the Git repo, including Gradle build files and basic Spring Boot application classes.

## Frontend

The frontend is a [React](https://reactjs.org/) web app that allows developers to create projects.  It includes a login page, a listing of completed project workflows, and a form to create a new project.

![Login Page](images/login.png)
![Create Project Page](images/create.png)
![Project Workflow Page](images/list.png)

# Architecture

## Logical Architecture

![Logical Architecture](images/logical-arch.jpg)

The logical architecture consists of a Spring Boot app, Orchestrator, that embeds the Flowable engine and all plugins.  All communication with external services uses HTTP.  Flowable stores all of it's artifacts in a PostgreSQL database.

## Deployment Architecture

![Deployment Architecture](images/deployment-arch.jpg)

The deployment architecture uses the development laptop to house the Orchestrator and the Flowable database.  Gitlab CE, Minikube, and Vault are running as services on a separate mini server running Ubuntu due to resource constraints on the laptop.

Gitlab is running in Docker on the mini server.  This is easiest way to get it running.  Minikube uses the `none` driver so that the Api Server can bind to the IP address of the mini server.  Minikube using the Docker driver complicates access to the Api Server due to the way networking works with the Docker driver.  Vault is running as a normal `systemd` service.

# Future Work

- Docker image creation via [Jib](https://github.com/GoogleContainerTools/jib)
- Docker repository creation
- Kubernetes deployment descriptors
- Monitoring profile and supporting configuration for [Graphite](https://graphiteapp.org/)
- gitlab-ci pipelines and secrets creation
